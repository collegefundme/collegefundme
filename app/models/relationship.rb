class Relationship < ApplicationRecord
  belongs_to :child
  belongs_to :contributor

  scope :donor_relationships, -> (contributor) { where(contributor: contributor) }

  enum name: {
	  Mother: 1,
	  Father: 2,
	  Grandmother: 3,
	  Uncle: 4,
	  Aunt: 5,
	  Sister: 6,
	  Brother: 7,
	  Familyfriend: 8
  }
end
