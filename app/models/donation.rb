class Donation < ApplicationRecord
  belongs_to :user
  belongs_to :child

  monetize :amount_cents, as: :amount
  validates :amount, numericality: { greater_than_or_equal_to: 1}

  scope :donated_to_students, -> (donor) { where(user_id: donor) }
  scope :donating_to_student, -> (student){ where(child_id: student)}
end
