class Child < ApplicationRecord
	include Tire::Model::Search
  include Tire::Model::Callbacks

  belongs_to :parent, class_name: 'User'
  has_many :relationships
  has_many :contributors, through: :relationships
  has_many :donations
  monetize :funding_goal_cents, as: :funding_goal
  monetize :donations_amount_cents, as: :donations_amount
  validates :first_name, :last_name, presence: true
  validates :user_name, presence: true, uniqueness: true

  has_attached_file :photo, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "default-avatar.png"
  validates_attachment :photo, content_type: { content_type: ["image/jpeg", "image/gif", "image/png"] }

  scope :my_sons, -> (user) { where(parent: user) }

  mapping do
		indexes :first_name, analyzer: 'snowball', boost: 200
		indexes :last_name, analyzer: 'snowball', boost: 200
    indexes :user_name, analyzer: 'snowball', boost: 200
	end

  def add_donation_amount(donated_amount)
    self.donations_amount = self.donations_amount + donated_amount
    self.save!(:validate => false)
  end

  def self.search(params)
	  tire.search(load: true) do
	    query { string params[:query]} if params[:query].present?
	  end
	end
  
end
