class Contributor < ApplicationRecord
  belongs_to :user
  has_many :relationships
  has_many :children, through: :relationships
  validates :email_address, presence: true, uniqueness: true
  validates :first_name, :last_name, presence: true

  scope :key_donors, -> (user) { where(user: user) }
  
  def full_name
    [first_name, last_name].join(' ')
  end
end
