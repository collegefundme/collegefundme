module UserPageHelper
	def donationPercentage(fundGoal, currentAmount)
		currentGoal = fundGoal * 1.0
		currentAmount = currentAmount * 1.0
        percentage_number = (currentAmount / currentGoal) * 100.0
        if percentage_number > 100
        	percentage_number = 100.to_s + '%'
       	else
        percentage_number.round.to_s + '%'	
        end
	end

	def relationship(value)
		if value == 1
			relation_name = "Mother"
		elsif value == 2
			relation_name = "Father"
		elsif value == 3
			relation_name = "Grandmother"
		elsif value == 4
			relation_name = "Uncle"
		elsif value == 5
			relation_name = "Aunt"
		elsif value == 6
			relation_name = "Sister"
		elsif value == 7
			relation_name = "Brother"
		elsif value == 8
			relation_name = "Family Friend"
		end
	end
end
