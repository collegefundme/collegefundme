class DonationsController < ApplicationController
  load_and_authorize_resource param_method: :donation_params
  before_action :set_donation, only: [:show, :edit, :update, :destroy]
  before_filter :find_child
  before_filter :authenticate_user!
 
  # GET /donations
  # GET /donations.json
  def index
    @donations = Donation.all
  end

  # GET /donations/1
  # GET /donations/1.json
  def show
  end

  # GET /donations/new
  def new
    @donation = Donation.new
  end

  # GET /donations/1/edit
  def edit
  end

  # POST /donations
  # POST /donations.json
  def create
    @user = current_user
    @donation = Donation.new(donation_params)
    @donation.amount.to_s.sub('$','')
    @donation.user_id = @user.id
    @donation.child_id = @child.id
    respond_to do |format|
      if @donation.save
        @child.add_donation_amount @donation.amount
        format.html { redirect_to child_path(@child), notice: 'Donation was successfully created.' }
        format.json { render :show, status: :created, location: @donation }
      else
        format.html { render :new }
        format.json { render json: @donation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /donations/1
  # PATCH/PUT /donations/1.json
  def update
    respond_to do |format|
      if @donation.update(donation_params)
        format.html { redirect_to @donation, notice: 'Donation was successfully updated.' }
        format.json { render :show, status: :ok, location: @donation }
      else
        format.html { render :edit }
        format.json { render json: @donation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /donations/1
  # DELETE /donations/1.json
  def destroy
    @donation.destroy
    respond_to do |format|
      format.html { redirect_to donations_url, notice: 'Donation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_donation
      @donation = Donation.find(params[:id])
    end

    def find_child
      if params[:child_id].present?
        @child = Child.find(params[:child_id])
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def donation_params
      params.require(:donation).permit(:amount, :user_id, :message, :child_id)
    end
end
