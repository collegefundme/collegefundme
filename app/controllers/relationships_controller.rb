class RelationshipsController < ApplicationController
	before_action :set_relationship, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!
  
  def index
    @relationships = Relationship.all
  end

  def show
  end

  def new
    @relationship = Relationship.new
  end

  def edit
  end

  def create
    @user = current_user
    @relationship = Child.new(child_params)

    respond_to do |format|
      if @relationship.save
        format.html { redirect_to root_path, notice: 'relationship was successfully created.' }
        format.json { render :show, status: :created, relationship: @relationship }
      else
        format.html { render :new }
        format.json { render json: @relationship.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @relationship.update(relationship_params)
        format.html { redirect_to @relationship, notice: 'relationship was successfully updated.' }
        format.json { render :show, status: :ok, relationship: @relationship }
      else
        format.html { render :edit }
        format.json { render json: @relationship.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @relationship.destroy
    respond_to do |format|
      format.html { redirect_to relationships_url, notice: 'relationship was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_relationship
      @relationship = Relationship.find(params[:id])
    end

    def relationship_params
      params.require(:relationship).permit(:name, :child_id, :contributor_id)
    end
end
