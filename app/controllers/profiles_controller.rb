class ProfilesController < ApplicationController

	def donor_profile
		@helped_students = []
		@donor = current_user if current_user.has_role? :donor
		@donations = Donation.donated_to_students(current_user)
		@donations.each do |donation|
			@helped_students << donation.child  
		end
	end

	def parent_profile
		@parent = current_user if current_user.has_role? :parent
	end

	def student_profile
		@student = Child.find_by(child_id: current_user.id) if current_user.has_role? :student
		@student_donors = []
	    @donations = Donation.donating_to_student(@student)
	    @donations.each do |donation|
	      @student_donors << donation.user  
	    end
	end

	def public_profile
		@user = User.find(params[:profile])
	end

end
