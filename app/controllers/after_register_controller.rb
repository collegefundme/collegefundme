class AfterRegisterController < Wicked::WizardController
	before_action :authenticate_user!
  steps :add_role, :about_you, :your_children, :contributor

  def show
    @step = step
    @steps = steps 
    @user = current_user
    if @user.has_role? :student
      if step == steps[2]
        redirect_to student_profile_path
      else
        render_wizard
      end
    elsif @user.has_role? :donor
      if step == steps[2]
        redirect_to donor_profile_path
      else
        render_wizard
      end
    elsif @user.has_role? :parent
      if step == steps[4]
        redirect_to parent_profile_path
      else  
        render_wizard
      end
    else
      render_wizard
    end
  end

  def update
   	@user = current_user
    case step
    when :about_you
      if params[:role_student] == 'Parent'
        @user.add_role :parent
      elsif params[:role_student] == 'Student'
        @user.add_role :student
      elsif params[:role_student] == 'Donor'
        @user.add_role :donor
      end
      if params[:address].present?
        @user.location = Location.new(address: params[:address])
      end
      @user.update_attributes(user_params)
      if @user.has_role? :student and !Child.exists?(child_id: @user.id)
        if params[:funding_goal].present?
          Child.create!(first_name: @user.first_name, last_name: @user.last_name , user_name: @user.user_name, email_address: @user.email, funding_goal: params[:funding_goal], birthday: @user.day_of_birth, child_id: @user.id, photo: @user.avatar)
        end
      end
    end
	  render_wizard @user
  end

  private

  def finish_wizard_path
    parent_profile_path
  end

  def user_params
    params.require(:user).permit(
      :first_name,
      :last_name,
      :user_name,
      :avatar,
      :day_of_birth,
      :email,
      :biography,
      location: [
        :address
      ],
      children_attributes: [
        :id, 
        :first_name, 
        :last_name,
        :user_name,
        :email_address,
        :photo,
        :_destroy
      ],
      contributors_attributes: [
        :id,
        :first_name,
        :last_name,
        :email_address,
        :_destroy
      ]
    )
  end
end
