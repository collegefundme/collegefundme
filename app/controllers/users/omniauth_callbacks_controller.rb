class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
 
 def self.provides_callback_for(provider)
    class_eval %Q{
      def #{provider}
        @user = User.find_for_oauth(env["omniauth.auth"], current_user)

        if @user.persisted?
          sign_in @user, event: :authentication
          if @user.has_role? :student or @user.has_role? :donor or @user.has_role? :parent
            redirect_to after_register_path(:about_you)
          else
            redirect_to after_register_path(:add_role)
          end
          
          set_flash_message(:notice, :success, kind: "#{provider}".capitalize) if is_navigational_format?
        else
          session["devise.#{provider}_data"] = env["omniauth.auth"]
          redirect_to new_user_registration_url
        end
      end
    }
  end

  [:stripe_connect, :facebook, :linked_in].each do |provider|
    provides_callback_for provider
  end
end
