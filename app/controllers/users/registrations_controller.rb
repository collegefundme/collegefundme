class Users::RegistrationsController < Devise::RegistrationsController
  before_filter :search_donor

  def create
    super
    if params[:donor].present? and params[:parent].present? and current_user.present?
      @user = current_user
      @donor = Contributor.find(params[:donor])
      parent = User.find(params[:parent])
      children = Child.where(parent_id: parent.id)
      @user.add_role :donor
      @donor.update(contributor_id: @user.id)
      children.each do |child|
        relationship = Relationship.new(name: @donor.relationship, contributor_id: @donor.id, child_id: child.id)
        relationship.save
      end
    end
  end

  protected
    def search_donor
      @donor = Contributor.find(params[:donor]) if params[:donor].present?
    end

    def after_sign_up_path_for(resource)
      if params[:donor].present?
        after_register_path(:about_you)
      else
        after_register_path(:add_role)
      end
    end
    
    def update_resource(resource, params)
      resource.update_without_password(params.except(:current_password))
    end
end
