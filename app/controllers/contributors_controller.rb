class ContributorsController < ApplicationController
	before_action :set_contributor, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!
  
  def index
    @user = current_user
    @contributors = Contributor.key_donors(@user)
  end

  def key_student
    @user = current_user
    @donor = Contributor.find_by(email_address: @user.email)
    @relationships = Relationship.donor_relationships(@donor) if @user.has_role? :donor
  end

  def show
    
  end

  def new
    
  end

  def edit
  end

  def create
    @user = current_user
    @contributor = Contributor.new(contributor_params)
    @contributor.user_id = @user.id

    respond_to do |format|
      if @contributor.save
        DonorMailer.donor_created(@contributor).deliver_later
        format.html { redirect_to after_register_path(:contributor), notice: 'Contributor was successfully created.' }
        format.json { render :show, status: :created, contributor: @contributor }
      else
        format.html { render :new }
        format.json { render json: @contributor.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @is_donor_registered = User.exists?(email: @contributor.email_address)
    respond_to do |format|
      if @contributor.update(contributor_params) and @is_donor_registered == false
        format.html { redirect_to @contributor, notice: 'Contributor was successfully updated.' }
        format.json { render :show, status: :ok, contributor: @contributor }
      else
        flash[:notice] = 'Contributor has already been registred'
        format.html { render :edit }
        format.json { render json: @contributor.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @contributor.destroy
    respond_to do |format|
      format.html { redirect_to contributors_url, notice: 'Contributor was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_contributor
      @contributor = Contributor.find(params[:id])
    end

    def contributor_params
      params.require(:contributor).permit(:first_name, :last_name, :email_address, :user_id, :relationship)
    end
end
