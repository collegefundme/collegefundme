class ChildrenController < ApplicationController
	before_action :set_child, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!
  
  def index
    @user = current_user
    @children = Child.my_sons(@user)
  end

  def child_search
    @children = Child.search(params)
  end

  def show
    #@amount = @child.donations.sum(:amount_cents)
    @amount = @child.donations_amount
    @amount_cents = @child.donations_amount_cents
    @student_donors = []
    @donations = Donation.donating_to_student(@child)
    @donations.each do |donation|
      @student_donors << donation.user  
    end
  end

  def new
    
  end

  def edit

  end

  def create
    @user = current_user
    @child = Child.new(child_params)
    @child.parent_id = @user.id
    @child.funding_goal.to_s.sub('$','')
    respond_to do |format|
      if @child.save
        format.html { redirect_to after_register_path(:your_children), notice: 'child was successfully created.' }
        format.json { render :show, status: :created, child: @child }
      else
        format.html { render :new }
        format.json { render json: @child.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @child.update(child_params)
        format.html { redirect_to @child, notice: 'child was successfully updated.' }
        format.json { render :show, status: :ok, child: @child }
      else
        format.html { render :edit }
        format.json { render json: @child.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @child.destroy
    respond_to do |format|
      format.html { redirect_to childs_url, notice: 'child was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_child
      @child = Child.find(params[:id])
    end

    def child_params
      params.require(:child).permit(:first_name, :last_name, :birthday, :user_name, :photo, :email_address, :parent_id, :funding_goal, :child_id)
    end
end
