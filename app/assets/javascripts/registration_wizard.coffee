renderWizardStep = ->
	$('.wizard-tabs a.tablinks').on 'click', (event) ->
		event.preventDefault()
		tabcontent = $(".tabcontent")
		for item in tabcontent
			$(item).hide()

		tablinks = $(".tablinks")
		for tab in tablinks
			$(tab).removeClass('active')

		containerName = $(@).data('container-name')
		$("##{containerName}").show()
		$(@).addClass("active")

$(document).ready renderWizardStep
$(document).on 'page:load', renderWizardStep
