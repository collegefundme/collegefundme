// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery2
//= require jquery_ujs
//= require jquery.turbolinks
//= require turbolinks
//= require turbolinks-compatibility
//= require foundation
//= require cocoon
// Bower packages
//= require autoNumeric/autoNumeric
//= require owl.carousel
//= require_tree .


// $(document).on('turbolinks:load', function() {
//  $(function(){ $(document).foundation(); });
// });

$(function(){ $(document).foundation(); });
$(document).on("page:load", function() { $(document).foundation(); });

$(function(){
    $(".owl-carousel").owlCarousel({
   navigation : false, // Show next and prev buttons
   slideSpeed : 2000,
   paginationSpeed : 2000,
   singleItem:true,
   autoPlay : 5000,
   pagination: false
   // "singleItem:true" is a shortcut for:
   // items : 1, 
   // itemsDesktop : false,
   // itemsDesktopSmall : false,
   // itemsTablet: false,
   // itemsMobile : false
 });
});
$(document).on("page:load", function() {
  $(".owl-carousel").owlCarousel({
    navigation : true, // Show next and prev buttons
    slideSpeed : 300,
    paginationSpeed : 400,
    singleItem:true
    // "singleItem:true" is a shortcut for:
    // items : 1, 
    // itemsDesktop : false,
    // itemsDesktopSmall : false,
    // itemsTablet: false,
    // itemsMobile : false
  });
});

function disable(i){
    $("#tab_"+i).prop("disabled",true);
}

$('tab1').on('click',function() {
    $(this).prop("disabled",true);
});
