class DonorMailer < ApplicationMailer
	default from: 'CollegeFundMe@gmail.com'
 
  def donor_created(donor)
    @donor = donor
    mail(to: @donor.email_address, subject: 'Welcome to My Awesome Site')
  end
end
