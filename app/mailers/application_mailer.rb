class ApplicationMailer < ActionMailer::Base
  default from: 'CollegeFundMe@gmail.com'
  layout 'mailer'
end
