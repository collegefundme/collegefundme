json.extract! location, :id, :latitude, :longitude, :address, :user_id, :created_at, :updated_at
json.url location_url(location, format: :json)