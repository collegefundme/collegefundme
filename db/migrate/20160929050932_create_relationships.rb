class CreateRelationships < ActiveRecord::Migration[5.0]
  def change
    create_table :relationships do |t|
      t.string :type
      t.references :child, foreign_key: true
      t.references :contributor, foreign_key: true

      t.timestamps
    end
  end
end
