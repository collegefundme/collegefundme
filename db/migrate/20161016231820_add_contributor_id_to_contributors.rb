class AddContributorIdToContributors < ActiveRecord::Migration[5.0]
  def change
  	add_column :contributors, :contributor_id, :integer
  end
end
