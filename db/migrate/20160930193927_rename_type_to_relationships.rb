class RenameTypeToRelationships < ActiveRecord::Migration[5.0]
  def change
  	rename_column :relationships, :type, :name
  end
end
