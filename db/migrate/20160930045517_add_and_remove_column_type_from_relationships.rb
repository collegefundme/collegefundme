class AddAndRemoveColumnTypeFromRelationships < ActiveRecord::Migration[5.0]
  def change
  	remove_column :relationships, :type
    add_column :relationships, :type, :integer
  end
end
