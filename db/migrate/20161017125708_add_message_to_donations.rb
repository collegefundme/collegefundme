class AddMessageToDonations < ActiveRecord::Migration[5.0]
  def change
  	add_column :donations, :message, :string
  end
end
