class RenameReferenceUserToChildren < ActiveRecord::Migration[5.0]
  def change
  	rename_column :children, :user_id, :parent_id
  end
end
