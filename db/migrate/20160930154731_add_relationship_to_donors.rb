class AddRelationshipToDonors < ActiveRecord::Migration[5.0]
  def change
  	add_column :contributors, :relationship, :integer
  end
end
