class RenameAmountToDonations < ActiveRecord::Migration[5.0]
  def change
  	rename_column :donations, :amount, :amount_cents
  	change_column :donations, :amount_cents, :integer
  end
end
