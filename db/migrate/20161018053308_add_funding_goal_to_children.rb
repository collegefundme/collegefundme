class AddFundingGoalToChildren < ActiveRecord::Migration[5.0]
  def change
  	add_column :children, :funding_goal_cents, :integer
  end
end
