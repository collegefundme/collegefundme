class NormalizeDonationAmounts < ActiveRecord::Migration[5.0]
  def change
  	add_column :children, :donations_amount_cents, :integer, default: 0
  end
end
