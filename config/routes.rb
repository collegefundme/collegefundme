Rails.application.routes.draw do
	# For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
	root to: "home#index"
	devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks', registrations: "users/registrations" }

	resources :after_register
  resources :locations
  resources :children do
    resources :donations
  end
  resources :contributors

  get 'donor/profile', to: 'profiles#donor_profile', as: 'donor_profile'
  get 'student/profile', to: 'profiles#student_profile', as: 'student_profile'
  get 'parent/profile', to: 'profiles#parent_profile', as: 'parent_profile'
  get 'public/profile', to: 'profiles#public_profile', as: 'public_profile'
  get 'child/search', to: 'children#child_search', as: 'child_search'
  get 'key_students', to: 'contributors#key_student', as: 'key_student'
	get 'user_page/show', to: 'user_page#show', as: 'user_page_show'
end
